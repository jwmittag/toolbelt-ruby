module Toolbelt
  module Fn
    Id = -> x { x }

    consts ||= {}
    define_method(:Const) do |x|
      consts[x] ||= ->*args { x }
    end

    module_function :Const

    True  = Const(true)
    False = Const(false)
  end
end
