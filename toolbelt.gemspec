# -*- encoding: utf-8 -*-
require_relative 'lib/toolbelt/version'

Gem::Specification.new do |s|
  s.name        = 'toolbelt'
  s.version     = Toolbelt::VERSION
  s.platform    = Gem::Platform::RUBY
  s.author      = 'Jörg W Mittag'
  s.email       = 'JoergWMittag+Ruby@GoogleMail.Com'
  s.homepage    = 'http://JoergWMittag.GitHub.Com/toolbelt-ruby/'
  s.summary     = 'Toolbelt is a set of monkey patches, utilities and other stuff that I repeatedly use in different projects.'
  s.description = 'Toolbelt is a set of monkey patches, utilities and other stuff that I repeatedly use in different projects.'

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end
